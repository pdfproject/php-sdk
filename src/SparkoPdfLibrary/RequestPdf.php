<?php

namespace PdfSparkoLibrary;
class RequestPdf
{
//{
//"html" : "<html lang='en'><head><meta charset='utf-8'><link href='bootstrap.css' rel='stylesheet'></head><body><div class='container'><div class='row'><h1 class='ui header' style='color: black; backgroud-color:red'>toto</h1></div></div></body></html>",
//"margins": "normale",
//"landscape": true,
//"format": "A4"
//}
    public const A3 = [11.7, 16.5];
    public const A4 = [8.27, 11.7];
    public const A5 = [5.8, 8.2];
    public const A6 = [4.1, 5.8];
    public const LETTER = [8.5, 11];
    public const LEGAL = [8.5, 14];
    public const TABLOID = [11, 17];

    public const NO_MARGINS = [0, 0, 0, 0];
    public const NORMAL_MARGINS = [1, 1, 1, 1];
    public const LARGE_MARGINS = [2, 2, 2, 2];

    private $html;
    private $margins;
    private $landscape = false;
    private $format;

    /**
     * @return mixed
     */
    public function getHtml()
    {
        return $this->html;
    }

    /**
     * @param mixed $html
     */
    public function setHtml($html): void
    {
        $this->html = $html;
    }

    /**
     * @return mixed
     */
    public function getMargins()
    {
        return $this->margins;
    }

    /**
     * @param mixed $margins
     */
    public function setMargins($margins): void
    {
        $this->margins = $margins;
    }

    /**
     * @return bool
     */
    public function isLandscape(): bool
    {
        return $this->landscape;
    }

    /**
     * @param bool $landscape
     */
    public function setLandscape(bool $landscape): void
    {
        $this->landscape = $landscape;
    }

    /**
     * @return mixed
     */
    public function getFormat()
    {
        return $this->format;
    }

    /**
     * @param mixed $format
     */
    public function setFormat($format): void
    {
        $this->format = $format;
    }



}