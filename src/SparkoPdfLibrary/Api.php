<?php

namespace PdfSparkoLibrary;
use \GuzzleHttp\RequestOptions;

class Api
{
    /**
     * @var string
     */
    private $apiKey;
    private static $instance;
    /**
     * @var \GuzzleHttp\Client
     */
    private $client;
    /**
     * @var array
     */
    private $headers;

    public function __construct(string $apiKey)
    {
        $this->apiKey = $apiKey;
        $this->client = new \GuzzleHttp\Client(['base_uri' => 'http://pdf.nicolas-r.fr/api/']);
        $this->headers = [
            'Authorization' => 'Bearer ' . $apiKey,
            'Accept'        => 'application/json',
        ];
    }
    public static function API(string $apiKey)
    {
        if (self::$instance === null) {
            self::$instance = new Api($apiKey);
        }

        return self::$instance;
    }

    private function post(array $data) {
        $response = $this->client->request('POST', 'convert', [
            'headers' => $this->headers,
                RequestOptions::JSON => $data

        ]);
        return $response->getBody()->getContents();
    }

    public function generatePdf(RequestPdf $pdf) {
        $data = [];
        $data['html'] = $pdf->getHtml();
        $data['margins'] = $pdf->getMargins();
        $data['format'] = $pdf->getFormat();

        return $this->post($data);
    }

    public function storePdf(RequestPdf $pdf, String $path) {
        $data = [];
        $data['html'] = $pdf->getHtml();
        $data['margins'] = $pdf->getMargins();
        $data['format'] = $pdf->getFormat();

        $resource = fopen($path, 'w');
        $response = $this->client->request('POST', 'convert', [
            'headers' => $this->headers,
            'sink' => $path,

                RequestOptions::JSON => $data

        ]);
        fclose($resource);
       return true;

    }

    

}