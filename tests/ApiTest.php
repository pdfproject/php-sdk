<?php

use PHPUnit\Framework\TestCase;
require dirname(__DIR__ ). "/vendor/autoload.php";
class ApiTest extends TestCase {

    public function testGenerator() {
        $pdfTest = new \PdfSparkoLibrary\Api("keyExample");
        $pdfRequestTest = new \PdfSparkoLibrary\RequestPdf();
        $pdfRequestTest->setLandscape(false);
        $pdfRequestTest->setHtml("<h1>toto</h1>h1>");
        $pdfRequestTest->setFormat(\PdfSparkoLibrary\RequestPdf::A4);

        $pdfTest->generatePdf($pdfRequestTest);

    }
}